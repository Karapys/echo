from main import app
from fastapi.testclient import TestClient


def test():
    with TestClient(app) as client:
        resp = client.get("/path")
        assert resp.status_code == 200
        assert resp.json() == "path"
