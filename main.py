from fastapi import FastAPI

app = FastAPI()


@app.get("/{url}")
def read_root(url):
    return str(url)
